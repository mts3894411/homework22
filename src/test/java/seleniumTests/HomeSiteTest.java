package seleniumTests;

import org.assertj.core.api.SoftAssertions;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

import static randomValue.RandomRussianAddress.generateRandomRussianAddress;
import static randomValue.RandomRussianFullName.generateRandomRussianFullName;
import static randomValue.RandomRussiаnRegion.generateRandomRussianRegion;

public class HomeSiteTest {
    public WebDriver driver;
    @Test
    @DisplayName("Тест-кейс A")
    public void testScenarioA() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        driver = new ChromeDriver(options);
        //--remote-allow-origins=* разрешает подключение из источника
        //https://groups.google.com/g/chromedriver-users/c/xL5-13_qGaA/m/cGk_5MvTAAAJ

        SoftAssertions softAssertions = new SoftAssertions();

        //1. Открыть ссылку https://homebrandofficial.ru/wear
        driver.get("https://homebrandofficial.ru/wear");

        //2. В строку поиска ввести "Лонгслив White&Green"
        By searchButton = By.xpath(
                "//*[name()='svg' and @class='t-store__filter__search-mob-btn-icon']");
        driver.findElement(searchButton).click();
        By queryField = By.xpath("//input[@name='query']");
        driver.findElement(queryField).sendKeys("Лонгслив White&Green");

        //3. Нажать на иконку “Поиск”
        By searchButton2 = By.xpath(
                "//input[@name='query']/following-sibling::*[name()='svg']");
        driver.findElement(searchButton2).click();

        //Негативная проверка количества найденного товара:
        By numberOfProducts = By.xpath("//span[@class=\"js-store-filters-prodsnumber\"]");
        String number = driver.findElement(numberOfProducts).getText();
        String expectedNumber = "2";
        softAssertions.assertThat(number).as("Неправильное количество найденного товара")
                .isEqualToIgnoringCase(expectedNumber);

        //Негативная проверка названия товара
        By productName = By.xpath("//div[text()='Лонгслив White&Green']");
        String realName = driver.findElement(productName).getText();
        String expectedName = "Лонгслив";
        softAssertions.assertThat(realName).as("Неправильное название товара")
                .isEqualToIgnoringCase(expectedName);

        //Позитивная проверка цены товара
        By productPrice = By.xpath("//div[@data-product-price-def=\"2800\"]");
        String realPrice = driver.findElement(productPrice).getText();
        String expectedPrice = "2 800";
        softAssertions.assertThat(realPrice).as("Правильная цена товара")
                .isEqualToIgnoringCase(expectedPrice);

        softAssertions.assertAll();
    }
    @Test
    @DisplayName("Тест-кейс B")
    public void testScenarioB() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        driver = new ChromeDriver(options);
        //--remote-allow-origins=* разрешает подключение из источника
        //https://groups.google.com/g/chromedriver-users/c/xL5-13_qGaA/m/cGk_5MvTAAAJ

        SoftAssertions softAssertions = new SoftAssertions();

        //1. Открыть ссылку https://homebrandofficial.ru/wear
        driver.get("https://homebrandofficial.ru/wear");

        //2. Нажать на товар с названием “Футболка Оversize”
        WebElement productButton = new WebDriverWait(driver, Duration.ofSeconds(10))
                .until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@href='https://" +
                        "homebrandofficial.ru/tproduct/544765431-930803998551-futbolka-oversize']")));
        productButton.click();

        //3. Нажать на кнопку “Добавить в корзину”
        By basketButton = By.className("js-store-prod-popup-buy-btn-txt");
        driver.findElement(basketButton).click();

        //4. Нажать на иконку "Корзина"
        By basketIcon = By.xpath("//div[@class='t706__carticon-imgwrap']");
        driver.findElement(basketIcon).click();

        //5. Нажать "Оформить заказ"
        By button = By.xpath("//button[text()='ОФОРМИТЬ ЗАКАЗ']");
        driver.findElement(button).click();

        //6. Заполнить все поля рандомными данными (!телефон +7 (000) 000-00-00)
        By nameButton = By.id("input_1496239431201");
        String randomFullName = generateRandomRussianFullName();
        driver.findElement(nameButton).sendKeys(randomFullName );

        By telButton = By.xpath("//input[@style='color: rgb(94, 94, 94);']");
        driver.findElement(telButton).sendKeys("0000000000");

        By regionButton = By.id("input_1627385047591");
        String randomRegion = generateRandomRussianRegion();
        driver.findElement(regionButton).sendKeys(randomRegion);

        By addressButton = By.id("input_1630305196291");
        String randomAddress = generateRandomRussianAddress();
        driver.findElement(addressButton).sendKeys(randomAddress);

        By radioButton = By.xpath("//input[@value='Самовывоз']/.. ");
        driver.findElement(radioButton).click();

        WebElement addressPickupButton = new WebDriverWait(driver, Duration.ofSeconds(10))
                .until(ExpectedConditions
                        .elementToBeClickable(By.xpath("//div[@id=\"pickup-searchbox\"]")));
        addressPickupButton.click();

        WebElement selectPickupButton = new WebDriverWait(driver, Duration.ofSeconds(10))
                .until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@data-name='Уфа']")));
        selectPickupButton.click();

        //7. Нажать кнопку “Оформить заказ”
        By checkoutButton = By.xpath("//button[@type='submit' and text()='ОФОРМИТЬ ЗАКАЗ']");
        driver.findElement(checkoutButton).click();

        //8. Проверить, что отображается текст
        // “Укажите, пожалуйста, корректный номер телефона” около поля "Телефон" и внизу страницы

        //Негативная проверка надписи об ошибке внизу страницы
        By errorButton = By.xpath("//p[text()='Укажите, пожалуйста, корректный номер телефона']");
        String realErrorButton = driver.findElement(errorButton).getText();
        String expectedErrorButton = "Введите корректный номер";
        softAssertions.assertThat(realErrorButton)
                .as("Неправильный текст ошибки")
                .isEqualToIgnoringCase(expectedErrorButton);

        //Негативная проверка надписи об ошибке около поля телефон
        By errorMessage = By.id("error_1496239478607");
        String realErrorMessage = driver.findElement(errorMessage).getText();
        String expectedErrorMessage = "Неверный телефон";
        softAssertions.assertThat(realErrorMessage)
                .as("Неправильный текст ошибки")
                .isEqualToIgnoringCase(expectedErrorMessage);

        softAssertions.assertAll();
    }


}

