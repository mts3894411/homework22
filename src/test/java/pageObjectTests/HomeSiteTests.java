package pageObjectTests;

import org.assertj.core.api.SoftAssertions;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import pageClasses.*;

import static randomValue.RandomRussianAddress.generateRandomRussianAddress;
import static randomValue.RandomRussianFullName.generateRandomRussianFullName;
import static randomValue.RandomRussiаnRegion.generateRandomRussianRegion;

public class HomeSiteTests {
     private WebDriver driver;
     private SearchLonqsleevePage searchLonqsleevePage;
     public ProductPageScenarioA productPageScenarioA;
     public OrderingPage orderingPage;
     public AddedToBasketPage addedToBasketPage;
     public CorrectMsgPage correctMsgPage;
    @Test
    @DisplayName("Тест-кейс A")
    public void testScenarioA() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        driver = new ChromeDriver(options);
        searchLonqsleevePage = new SearchLonqsleevePage(driver);
        productPageScenarioA = new ProductPageScenarioA(driver);

        //--remote-allow-origins=* разрешает подключение из источника
        //https://groups.google.com/g/chromedriver-users/c/xL5-13_qGaA/m/cGk_5MvTAAAJ

        SoftAssertions softAssertions = new SoftAssertions();

        //1. Открыть ссылку https://homebrandofficial.ru/wear
        //2. В строку поиска ввести "Лонгслив White&Green"
        //3. Нажать на иконку “Поиск”

        searchLonqsleevePage.openHomePage();
        String name = "Лонгслив White&Green";
        searchLonqsleevePage.clickSearchButton()
                .setQueryField(name)
                .clickSearchButton2();

        //Негативная проверка количества найденного товара:
        String expectedNumber = "2";
        productPageScenarioA.numberShouldBe(softAssertions,expectedNumber);

        //Негативная проверка названия товара
        String expectedName = "Лонгслив";
        productPageScenarioA.nameShouldBe(softAssertions, expectedName);

        //Позитивная проверка цены товара
        String expectedPrice = "2 800";
        productPageScenarioA.priceShouldBe(softAssertions, expectedPrice);

        softAssertions.assertAll();
    }

    @Test
    @DisplayName("Тест-кейс B")
    public void testScenarioB() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        driver = new ChromeDriver(options);
        searchLonqsleevePage = new SearchLonqsleevePage(driver);
        orderingPage = new OrderingPage(driver);
        addedToBasketPage = new AddedToBasketPage(driver);
        correctMsgPage = new CorrectMsgPage(driver);

        //--remote-allow-origins=* разрешает подключение из источника
        //https://groups.google.com/g/chromedriver-users/c/xL5-13_qGaA/m/cGk_5MvTAAAJ

        SoftAssertions softAssertions = new SoftAssertions();

        //1. Открыть ссылку https://homebrandofficial.ru/wear
        //2. Нажать на товар с названием "Футболка Оversize"
        //3. Нажать на кнопку "Добавить в корзину"
        //4. Нажать на иконку "Корзина"
        //5. Нажать "Оформить заказ"

        searchLonqsleevePage.openHomePage();
        addedToBasketPage.waitProductButton().clickProductButton();
        addedToBasketPage.clickBasketButton();
        orderingPage.clickBasketIcon().clickButton();

        //6. Заполнить все поля рандомными данными (!телефон +7 (000) 000-00-00)

        String randomFullName = generateRandomRussianFullName();
        String randomRegion = generateRandomRussianRegion();
        String randomAddress = generateRandomRussianAddress();
        orderingPage.setNameButton(randomFullName)
                .setTelButton()
                .setRegionButton(randomRegion)
                .setAddressButton(randomAddress);
        orderingPage.clickRadioButton();
        orderingPage.waitAddressPickupButton()
                .clickAddressPickupButton();
        orderingPage.waitSelectPickupButton()
                .clickSelectPickupButton();

        //7. Нажать кнопку “Оформить заказ”
        orderingPage.clickCheckoutButton();

        //8. Проверить, что отображается текст
        // “Укажите, пожалуйста, корректный номер телефона” около поля "Телефон" и внизу страницы

        //Негативная проверка надписи об ошибке внизу страницы
        String expectedErrorButton = "Введите корректный номер";
        correctMsgPage.buttonShouldBe(softAssertions,expectedErrorButton);

        //Негативная проверка надписи об ошибке около поля телефон
        String expectedErrorMessage = "Неверный телефон";
        correctMsgPage.messageShouldBe(softAssertions,expectedErrorMessage);

        softAssertions.assertAll();
    }
}
