package pageClasses;

import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CorrectMsgPage {
     WebDriver driver;
     @FindBy(xpath = "//p[text()='Укажите, пожалуйста, корректный номер телефона']")
     WebElement errorButton;
     @FindBy(id = "error_1496239478607")
     WebElement errorMessage;
    public CorrectMsgPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void buttonShouldBe(SoftAssertions softAssertions, String  expectedErrorButton){
        softAssertions.assertThat(errorButton.getText())
                .as("Неправильный текст ошибки")
                .isEqualToIgnoringCase(expectedErrorButton);
    }

    public void messageShouldBe(SoftAssertions softAssertions, String  expectedErrorMessage){
        softAssertions.assertThat(errorMessage.getText())
                .as("Неправильный текст ошибки")
                .isEqualToIgnoringCase(expectedErrorMessage);
    }
}
