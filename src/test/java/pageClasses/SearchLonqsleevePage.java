package pageClasses;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SearchLonqsleevePage {

    @FindBy(xpath = "//*[name()='svg' and @class='t-store__filter__search-mob-btn-icon']")
    private WebElement searchButton;
    @FindBy(xpath = "//input[@name='query']")
    private WebElement queryField;
    @FindBy(xpath = "//input[@name='query']/following-sibling::*[name()='svg']")
    private WebElement searchButton2;
    private WebDriver  driver;
    public SearchLonqsleevePage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void openHomePage(){
        driver.get("https://homebrandofficial.ru/wear");
    }

    public SearchLonqsleevePage clickSearchButton(){
        searchButton.click();
        return this;
    }

    public SearchLonqsleevePage setQueryField(String name){
        queryField.sendKeys(name);
        return this;
    }

    public SearchLonqsleevePage clickSearchButton2(){
        searchButton2.click();
        return this;
    }
}
