package pageClasses;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class AddedToBasketPage {
    WebDriver driver;

    @FindBy(xpath = "//div[text()='Футболка Оversize']")
    WebElement productButton;
    static final By BY_PRODUCT_BUTTON = By.xpath("//div[text()='Футболка Оversize']");
    @FindBy(className = "js-store-prod-popup-buy-btn-txt")
    WebElement basketButton;

    public AddedToBasketPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public AddedToBasketPage waitProductButton(){
        new WebDriverWait(driver, Duration.ofSeconds(10))
                .until(ExpectedConditions
                        .elementToBeClickable(BY_PRODUCT_BUTTON));
        return this;
    }

    public AddedToBasketPage clickProductButton(){
        productButton.click();
        return this;
    }

    public AddedToBasketPage clickBasketButton(){
        basketButton.click();
        return this;
    }
}
