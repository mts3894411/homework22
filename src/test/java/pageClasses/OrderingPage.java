package pageClasses;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class OrderingPage {
    WebDriver driver;

    @FindBy(xpath = "//div[@class='t706__carticon-imgwrap']")
    WebElement basketIcon;
    @FindBy(xpath = "//button[text()='ОФОРМИТЬ ЗАКАЗ']")
    WebElement button;
    @FindBy(id = "input_1496239431201")
    WebElement nameButton;
    @FindBy(xpath = "//input[@style='color: rgb(94, 94, 94);']")
    WebElement telButton;
    @FindBy(id = "input_1627385047591")
    WebElement regionButton;
    @FindBy(id = "input_1630305196291")
    WebElement addressButton;
    @FindBy(xpath = "//input[@value='Самовывоз']/.. ")
    WebElement radioButton;
    @FindBy(xpath = "//div[@id='pickup-searchbox']")
    WebElement addressPickupButton;
    //static final By BY_ADDRESS_BUTTON = By.xpath("//div[@id='pickup-searchbox']");
    @FindBy(xpath = "//div[@data-name='Уфа']")
    WebElement selectPickupButton;
    //static final By BY_SELECT_BUTTON = By.xpath("//div[@data-name='Уфа']");
    @FindBy(xpath = "//button[@type='submit' and text()='ОФОРМИТЬ ЗАКАЗ']")
    WebElement checkoutButton;

    public OrderingPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public OrderingPage clickBasketIcon(){
        basketIcon.click();
        return this;
    }

    public OrderingPage clickButton(){
        button.click();
        return this;
    }

    public OrderingPage setNameButton(String randomFullName){
        nameButton.sendKeys(randomFullName);
        return this;
    }

    public OrderingPage setTelButton(){
        telButton.sendKeys("0000000000");
        return this;
    }

    public OrderingPage setRegionButton(String randomRegion){
        regionButton.sendKeys(randomRegion);
        return this;
    }

    public OrderingPage setAddressButton(String randomAddress){
        addressButton.sendKeys(randomAddress);
        return this;
    }

    public OrderingPage clickRadioButton(){
        radioButton.click();
        return this;
    }

    public OrderingPage waitAddressPickupButton(){
        new WebDriverWait(driver, Duration.ofSeconds(10))
                .until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@id='pickup-searchbox']")));
        return this;
    }

    public OrderingPage clickAddressPickupButton(){
        addressPickupButton.click();
        return this;
    }

    public OrderingPage waitSelectPickupButton(){
        new WebDriverWait(driver, Duration.ofSeconds(10))
                .until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@data-name='Уфа']")));
        return this;
    }

    public OrderingPage clickSelectPickupButton(){
        selectPickupButton.click();
        return this;
    }

    public OrderingPage clickCheckoutButton(){
        checkoutButton.click();
        return this;
    }
}
