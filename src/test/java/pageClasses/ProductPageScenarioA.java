package pageClasses;

import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ProductPageScenarioA {
    @FindBy(xpath = "//span[@class='js-store-filters-prodsnumber']")
    WebElement numberOfProducts;
    @FindBy(xpath = "//div[text()='Лонгслив White&Green']")
    WebElement productName;
    @FindBy(xpath = "//div[@data-product-price-def='2800']")
    WebElement productPrice;

    private final WebDriver driver;
    public ProductPageScenarioA(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public String getTitle(){
        return driver.getTitle();
    }

    public void numberShouldBe(SoftAssertions softAssertions, String  expectedNumber){
        softAssertions.assertThat(numberOfProducts.getText())
                .as("Неправильное количество найденного товара")
                .isEqualToIgnoringCase(expectedNumber);
    }

    public void nameShouldBe(SoftAssertions softAssertions, String expectedName){
        softAssertions.assertThat(productName.getText())
                .as("Неправильное название товара")
                .isEqualToIgnoringCase(expectedName);
    }

    public void priceShouldBe(SoftAssertions softAssertions, String expectedPrice){
        softAssertions.assertThat(productPrice.getText())
                .as("Правильная цена товара")
                .isEqualToIgnoringCase(expectedPrice);
    }
}
