package randomValue;

import java.util.Random;

public class RandomRussiаnRegion {
    private static final String[] REGIONS = {"Московская область",
            "Ленинградская область", "Татарстан", "Свердловская область",
            "Ростовская область", "Нижегородская область", "Краснодарский край",
            "Самарская область", "Пермский край", "Челябинская область"};

    public static void main(String[] args) {
        String randomRegion = generateRandomRussianRegion();
        System.out.println(randomRegion);
    }

    public static String generateRandomRussianRegion() {
        Random random = new Random();
        String region = REGIONS[random.nextInt(REGIONS.length)];
        return region;
    }
}
