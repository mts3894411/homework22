package randomValue;

import java.util.Random;

public class RandomRussianFullName {
    private static final String[] FIRST_NAMES_MALE = {"Иван", "Александр", "Дмитрий", "Михаил", "Сергей", "Андрей", "Юрий", "Николай", "Владимир", "Павел"};
    private static final String[] FIRST_NAMES_FEMALE = {"Анна", "Екатерина", "Мария", "Ольга", "Татьяна", "Наталья", "Светлана", "Ирина", "Юлия", "Елена"};
    private static final String[] LAST_NAMES_MALE = {"Иванов", "Петров", "Сидоров", "Смирнов", "Кузнецов", "Соколов", "Михайлов", "Новиков", "Федоров", "Морозов"};
    private static final String[] LAST_NAMES_FEMALE = {"Иванова", "Петрова", "Сидорова", "Смирнова", "Кузнецова", "Соколова", "Михайлова", "Новикова", "Федорова", "Морозова"};
    private static final String[] MIDDLE_NAMES_MALE = {"Александрович", "Дмитриевич", "Михайлович", "Сергеевич", "Андреевич", "Юрьевич", "Николаевич", "Владимирович", "Павлович", "Геннадьевич"};
    private static final String[] MIDDLE_NAMES_FEMALE = {"Александровна", "Дмитриевна", "Михайловна", "Сергеевна", "Андреевна", "Юрьевна", "Николаевна", "Владимировна", "Павловна", "Геннадьевна"};

    public static void main(String[] args) {
        String randomFullName = generateRandomRussianFullName();
        System.out.println(randomFullName);
    }

    public static String generateRandomRussianFullName() {
        Random random = new Random();
        String firstName, lastName, middleName;
        boolean isMale = random.nextBoolean();

        if (isMale) {
            firstName = FIRST_NAMES_MALE[random.nextInt(FIRST_NAMES_MALE.length)];
            middleName = MIDDLE_NAMES_MALE[random.nextInt(MIDDLE_NAMES_MALE.length)];
            lastName = LAST_NAMES_MALE[random.nextInt(LAST_NAMES_MALE.length)];
        } else {
            firstName = FIRST_NAMES_FEMALE[random.nextInt(FIRST_NAMES_FEMALE.length)];
            middleName = MIDDLE_NAMES_FEMALE[random.nextInt(MIDDLE_NAMES_FEMALE.length)];
            lastName = LAST_NAMES_FEMALE[random.nextInt(LAST_NAMES_FEMALE.length)];
        }

        return String.format("%s %s %s", lastName, firstName, middleName);
    }
}
