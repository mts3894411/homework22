package randomValue;

import java.util.Random;

public class RandomRussianAddress {
    private static final String[] CITIES = {"Москва", "Санкт-Петербург",
            "Новосибирск", "Екатеринбург", "Казань", "Нижний Новгород",
            "Челябинск", "Самара", "Омск", "Ростов-на-Дону"};
    private static final String[] STREETS = {"Ленина", "Кирова",
            "Советская", "Гагарина", "Пушкина", "Мира", "Красноармейская",
            "Маяковского", "Куйбышева", "Октябрьская"};

    public static void main(String[] args) {
        String randomAddress = generateRandomRussianAddress();
        System.out.println(randomAddress);
    }

    public static String generateRandomRussianAddress() {
        Random random = new Random();
        String city = CITIES[random.nextInt(CITIES.length)];
        String street = STREETS[random.nextInt(STREETS.length)];
        int homeNumber = random.nextInt(100) + 1;
        int apartmentNumber = random.nextInt(100) + 1;
        return String.format("%s, %s ул., %d, кв. %d", city, street, homeNumber, apartmentNumber);
    }
}
